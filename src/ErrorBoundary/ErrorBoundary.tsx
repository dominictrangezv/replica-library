// Multivariate Dependencies
import React, { PropsWithChildren, ErrorInfo } from 'react';

// Types
import type { ErrorBoundaryProps, ErrorBoundaryState } from './types/ErrorBoundaryProps';

/**
 * @deprecated Do not use legacy components anymore.
 */
class ErrorBoundary extends React.Component<PropsWithChildren<ErrorBoundaryProps>, ErrorBoundaryState> {
  constructor(props: PropsWithChildren<ErrorBoundaryProps>) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, info: ErrorInfo) {
    this.setState({ hasError: true });
    const { onError } = this.props;
    if (onError) {
      onError(error, info);
    }
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    return hasError ? null : children;
  }
}

export default ErrorBoundary;
