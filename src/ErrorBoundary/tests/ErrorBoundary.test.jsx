// Multivariate dependencies
import React, { useState } from 'react';

// Utils
import { mount } from 'enzyme';

// Components
import ErrorBoundary from '../ErrorBoundary';

const ClickErrorTester = ({ text }) => {
  const [clicked, setClickState] = useState(false);

  const onClick = () => setClickState(true);

  if (clicked) {
    throw new Error('Oh no!');
  }
  return (
    <button type='button' onClick={onClick}>
      {text}
    </button>
  );
};

let wrapped;
let errorOccurred;
const markErrorOccurred = () => {
  errorOccurred = true;
};

beforeEach(() => {
  errorOccurred = false;
  wrapped = mount(
    <ErrorBoundary onError={markErrorOccurred}>
      <ClickErrorTester text='test' />
    </ErrorBoundary>
  );
});

afterEach(() => {
  wrapped.unmount();
});

it('shows error boundary', () => {
  expect(wrapped.find(ErrorBoundary).length).toEqual(1);
});

it('shows test text', () => {
  expect(wrapped.text()).toEqual('test');
});

it('does not show ClickErrorTester on click', () => {
  // To suppress errors from console
  jest.spyOn(console, 'error');
  wrapped.find(ClickErrorTester).first().simulate('click');
  expect(wrapped.find(ClickErrorTester).length).toEqual(0);
  expect(errorOccurred).toEqual(true);
});
