// Types
import type { ErrorInfo } from 'react';

/**
 * @deprecated Do not use legacy components anymore.
 */
export interface ErrorBoundaryProps {
  onError: (error: Error, info: ErrorInfo) => void;
  key?: number;
}

/**
 * @deprecated Do not use legacy components anymore.
 */
export interface ErrorBoundaryState {
  hasError: boolean;
}
